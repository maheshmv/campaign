**Clone**

git clone git@bitbucket.org:maheshmv/campaign.git

**Restore packages**

You should open the Campaign.sln file on VS 2017.
Right click on the Solution in the Solutions Explorer, and click Restore Nuget packages.


**FAQ**

1. There is nothing in Program.cs, I called the individual class procedures from here for my debugging.
2. You can use all other cs file for verification of the answers to given questions.
3. Each cs file (other than Progam.cs) is numbered as per the question number.
4. You can test the files using the unit test cases project for each file.
5. SQL queries can be found in 5_6_SQLAnswers.sql which has answers to question 5 and 6.



