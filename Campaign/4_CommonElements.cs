﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Campaign
{
    public class CommonElements
    {
        private static int[] arr;

        // Logic applied below is to Create the list of elements having occurence 
        // equal to element with max occurences
        public static int[] FindCommonElements(int[] array)
        {
            if (array is null)
                return new int[] { };
            arr = array;
            Dictionary<int, int> dict = new Dictionary<int, int>();
            InserttoDictionary(dict);
            int max = 1;
            List<int> retList = new List<int>();
           foreach(KeyValuePair<int,int> pair in dict)
            {
                if(pair.Value > max)
                {
                    // ignore previous key, create new list of common keys
                    retList = new List<int> { pair.Key };
                    max = pair.Value;
                }
                else if (pair.Value == max)
                {
                    retList.Add(pair.Key);
                }
            }
            //retList.Sort();
            return retList.ToArray();
        }

        private static void InserttoDictionary(Dictionary<int, int> dict)
        {
            foreach (int i in arr)
            {
                if (dict.ContainsKey(i))
                {
                    dict[i]++;
                }
                else
                {
                    dict.Add(i, 1);
                }

            }
        }
    }
}
