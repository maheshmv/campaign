﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * Using the most appropriate means, implement an "isNullOrEmpty" check on the standard String type.
 * It should be functionally equivalent without calling any "isNullOrEmpty" built in function.
*/
namespace Campaign
{
    public static class MyString
    {

        //public static bool MyIsNullOrEmpty(this String s, string Str)
        public static bool IsNullOrEmpty(string Str)
        {
            string str = Str;
            bool ret = true;
            if (//str == null || 
                str?.Length > 0)
            {
                ret = false;
            }
            return ret;
        }
    }
}
