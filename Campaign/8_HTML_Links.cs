﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using System.Threading.Tasks;
using System.Text;
using System.Net;
using System.IO;

namespace Campaign
{
    public class HTML_Links
    {
        public static List<string> GetLinkURLs(string text)
        {
            HtmlDocument doc = new HtmlDocument();
            //string text = File.ReadAllText(@"..\..\file.html", Encoding.UTF8);
            if(text is null)
            {
                return null;
            }
            List<string> allLinksOrAnchors = new List<string>();

            doc.LoadHtml(text);
            var nodes = doc.DocumentNode.SelectNodes("//a");// add anchor tags
            if (nodes != null)
            {
                allLinksOrAnchors = nodes.Select((x) => { return x.Attributes["href"].Value; }).ToList();
            }

            nodes = doc.DocumentNode.SelectNodes("//link");  // add link tags
            if (nodes != null)
            {
                allLinksOrAnchors.AddRange(nodes.Select((x) => { return x.Attributes["href"].Value; }).ToList());
            }

            List<Task> taskList = new List<Task>();
            foreach (var Url in allLinksOrAnchors)
            {

                //var task = Task.Factory.StartNew
                taskList.Add(Task.Factory.StartNew(() =>
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);

                    try
                    {
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Console.WriteLine($"Response of \"{Url}\" : {response.StatusCode}");
                    }
                    catch (WebException ex)
                    { Console.WriteLine($"WebException occurred in {Url}: [{ex.Response}] {ex.Message}"); }
                    catch (Exception ex)
                    { Console.WriteLine(ex.Message); }
                }));
            }
            Task.WaitAll(taskList.ToArray());

            return allLinksOrAnchors;
        }
    }
}
