﻿

--Question 4:

-- SalesPerson having orders with George
Select DISTINCT sp.Name from @SalesPerson sp
INNER JOIN @Orders o on sp.SalespersonID = o.SalespersonID
INNER JOIN @Customer cust on cust.CustomerID = o.CustomerID and cust.Name = 'George'

-- SalesPerson Not having orders with George
Select DISTINCT sp.Name from @SalesPerson sp
LEFT JOIN @Orders o on sp.SalespersonID = o.SalespersonID
LEFT JOIN @Customer cust on cust.CustomerID = o.CustomerID 
WHERE sp.SalespersonID NOT IN ( Select DISTINCT sp.SalespersonID from @SalesPerson sp
INNER JOIN @Orders o on o.SalespersonID=sp.SalespersonID 
INNER JOIN @Customer cust on cust.CustomerID = o.CustomerID and cust.Name = 'George'  )

-- Salespersons having 2 or more orders
Select sp.Name, count(o.OrderID) NumOfOrders from @Orders o
INNER JOIN @SalesPerson sp on sp.SalespersonID = o.SalespersonID
Group by sp.Name
having count(o.OrderID)>1  -- 2 or more orders

--======================================================================================================
--Question 5:

-- name of the salesperson with the 3rd highest salary.
DECLARE @NthHighest as int = 3
SELECT TOP 1 SP.Name FROM @SalesPerson SP
WHERE SP.SalespersonID NOT IN (
SELECT TOP (@NthHighest-1) S.SalespersonID FROM @SalesPerson S
ORDER BY S.Salary DESC
) ORDER BY SP.Salary DESC

/* Create a new roll­up table BigOrders(where columns are CustomerID,
   TotalOrderValue), and insert into that table customers whose total Amount across all
    orders is greater than 1000
*/

declare @BigOrders as table
(
  CustomerID  int,
  TotalOrderValue int
)
INSERT INTO @BigOrders
SELECT C.Name, SUM(O.CostOfUnit) Total
 FROM @Customer C
JOIN @Orders O ON C.CustomerID = O.CustomerID
GROUP BY C.Name
HAVING SUM(O.CostOfUnit)>1000

--total Amount of orders for each month, ordered by year, then month
select FORMAT(O.OrderDate, 'MMM-yy') [Month-Year], SUM(O.CostOfUnit) [Total Amount]
 from @Orders O
 GROUP BY YEAR(O.OrderDate) , MONTH(O.OrderDate)  ,FORMAT(O.OrderDate, 'MMM-yy')
 ORDER BY  YEAR(O.OrderDate) DESC, MONTH(O.OrderDate) DESC ,FORMAT(O.OrderDate, 'MMM-yy')