﻿using System;
using System.Collections.Generic;
using System.Linq;

public class MyMath
{
    public static List<int> Divisors(int Num)
    {
        List<int> list = new List<int>();

        if (Num <= 0)
            return list;
        if (Num == 1)
            return new List<int>() { 1 };


        for (int divisor=1; divisor <= Num/2 && !list.Contains(divisor); divisor++)
        {
            int dividend = Num / divisor;
            if (Num % divisor == 0)
            {
                list.Add(divisor);
                if(divisor != dividend)
                    list.Add(dividend);
            }
        }

         list.Sort(); return list;
    }
    private static bool IsPrime(int n)
    {

        if (n == 0 || n == 1 || n == 2)
            return true ;
        double limit = Math.Sqrt(n);

        for(int i =3;i<=limit;i++)
        {
            if(n%i==0)
            {
                return false;
            }
        }

        return true;
    }
}