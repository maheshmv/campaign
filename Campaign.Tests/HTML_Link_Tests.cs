﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Campaign.Tests
{
    public class HTML_Link_Tests
    {

        [Fact]
        void NullHTML()
        {
            string html = null;
            List<string> urls = HTML_Links.GetLinkURLs(html);
            Assert.Null(urls);
        }
        [Fact]
        void SingleLinkHTML()
        {
            string html = $"<a href=\"https://www.google.com\">click</a>";
            List<string> urls = HTML_Links.GetLinkURLs(html);
            Assert.Equal(1, urls.Count);
        }
        [Fact]
        void EmptystringAsHTML()
        {
            string html = $"";
            List<string> urls = HTML_Links.GetLinkURLs(html);
            Assert.Empty(urls);
        }
        [Fact]
        void LargeHTMLCode()
        {
            string html = $"<html lang=\"en\"> <head>    <meta charset=\"utf-8\">    <link rel=\"dns-prefetch\" href=\"https://github.githubassets.com\">     <link rel=\"dns-prefetch\" href=\"https://docs.microsoft.com/\">     <link rel=\"dns-prefetch\" href=\"https://stackoverflow.com/\">     <link rel=\"dns-prefetch\" href=\"https://www.twitter.com/\">     <link rel=\"dns-prefetch\" href=\"https://docs.microsoft.com/en-us/netdot/\">     <link rel=\"dns-prefetch\" href=\"http://www.ibnlivemint.com/\">     <link rel=\"dns-prefetch\" href=\"https://hash.online-convert.com/sha1-generator\"> </head> <body>     <div class=\"Header-item d-none d-lg-flex\">         <a class=\"Header-link\" href=\"https://github.com/\" data-hotkey=\"g d\" aria-label=\"Homepage\" data-ga-click=\"Header, go to dashboard, icon:logo\">             <svg class=\"octicon octicon-mark-github v-align-middle\" height=\"32\" viewBox=\"0 0 16 16\" version=\"1.1\" width=\"32\" aria-hidden=\"true\"><path fill-rule=\"evenodd\" d=\"M8 0C3.58 \" /></ svg >         </ a >      </ div >      < div class=\"Header-item d-lg-none\">         <button class=\"Header-link btn-link js-details-target\" type=\"button\" aria-label=\"Toggle navigation\" aria-expanded=\"false\">             <svg height=\"24\" class=\"octicon octicon-three-bars\" viewBox=\"0 0 12 16\" version=\"1.1\" width=\"18\" aria-hidden=\"true\"><path fill-rule=\"evenodd\" d=\"M11\" /></svg>         </button>     </div>     <a  href= \"https://www.qwiklabs.com/users/sign_in\"> Qwikliabs </a> </body> </html>  ";
            List<string> urls = HTML_Links.GetLinkURLs(html);
            Assert.Equal(9, urls.Count);
        }
    }
}
