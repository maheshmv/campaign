﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Campaign.Tests
{
    public class DivisorTests
    {
        List<int> list = new List<int>();
        [Fact]
        void ValidNumCheckAllDivides()
        {
            int num = 84;
            list = MyMath.Divisors(num);
            Assert.All(list, (i) =>
            {
                int r = num % i;
                Assert.Equal(0, r);
            });
        }
        [Fact]
        void CheckZeroAndNegative()
        {
            int num = 0;
            list = MyMath.Divisors(num);
            Assert.Empty(list);

            num = -180;
            list = MyMath.Divisors(num);
            Assert.Empty(list);
        }
        [Fact]
        void CheckOne()
        {
            int num = 1;
            list = MyMath.Divisors(num);
            Assert.Single(list);
            Assert.Equal(1, list.First());
        }
        [Fact]
        void CheckTwo()
        {
            int num = 2;
            list = MyMath.Divisors(num);
            
            Assert.Equal(2, list.Count);
        }
        [Fact]
        void CheckSmallNum()
        {
            int num = 10;
            list = MyMath.Divisors(num);
            Assert.Equal(4, list.Count); // 1,2,5,10
        }
        [Fact]
        void CheckPrimes()
        {
            int num = 7;
            list = MyMath.Divisors(num);
            Assert.Equal(2, list.Count); // 1,7
            num = 4673;
            list = MyMath.Divisors(num);
            Assert.Equal(2, list.Count); // 1,4673
        }
    }
}
