﻿using Xunit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Campaign;

namespace Campaign.Tests
{
    public class MyStringTests
    {
        [Fact]
        void NullCheck()
        {
            string str = null;
            Assert.True(MyString.IsNullOrEmpty(str));
        }
        [Fact]
        void ValidStringCheck()
        {
            string str = "Foo";
            Assert.False(MyString.IsNullOrEmpty(str));
        }
        [Fact]
        void EmptyStringCheck()
        {
            string str = "";
            Assert.True(MyString.IsNullOrEmpty(str));
        }
        [Fact]
        void NULLStringCheck()
        {
            string str = "null";
            Assert.False(MyString.IsNullOrEmpty(str));
        }
        [Fact]
        void UninitializedConcatenatedStringCheck()
        {
            string str = "" + "";
            String newStr = str;
            Assert.True(MyString.IsNullOrEmpty(str));
            Assert.True(MyString.IsNullOrEmpty(newStr));
        }
        [Fact]
        void IntToStringConvertCheck()
        {
            int str = 0 ;
            Nullable<int> MyInt = null;
            Assert.False(MyString.IsNullOrEmpty(Convert.ToString(str)));
            Assert.True (MyString.IsNullOrEmpty(MyInt.ToString()));
        }
    }
}
