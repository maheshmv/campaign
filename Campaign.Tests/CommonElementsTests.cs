﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

using System.Threading.Tasks;

namespace Campaign.Tests
{
    public class CommonElementsTests
    {
        [Fact]
        void RepeatedElements()
        {
            int[] arr = new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6 }; //5
            int[] ret = CommonElements.FindCommonElements(arr);
            Assert.Collection(ret, i => { Assert.Equal(5, i); });
        }
        [Fact]
        void AllSingularElements()
        {
            int[] arr = new int[] { 1, 2, 3, 4, 5 };// All
            int[] ret = CommonElements.FindCommonElements(arr);
            Assert.Equal(5, ret.Length);
        }

        [Fact]
        void TwoCommonElements()
        {
            int[] arr = new int[] { 6, 2, 3, 4, 5, 1, 6, 4 };// 6,4
            int[] ret = CommonElements.FindCommonElements(arr);
            Assert.Equal(2, ret.Length);

        }

        [Fact]
        void NullArray()
        {
            int[] arr = null;
            int[] ret = CommonElements.FindCommonElements(arr);
            Assert.Empty(ret);
        }

        [Fact]
        void EmptyArray()
        {
            int[] arr = new int[] { };
            int[] ret = CommonElements.FindCommonElements(arr);
            Assert.Empty(ret);
        }
    }
}
